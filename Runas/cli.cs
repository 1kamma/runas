using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
namespace Runas
{
    internal class CLI
    {
        [Option('f',"file", Required=false, HelpText="add full name of the file")]
        public string FileName { get; set; }
        [Option('a', "argm", Required = false, HelpText ="argument for the file")]
        public string Arguments { get; set; }
        [Option("runas", Default =false,Required =false)]
        public bool runas { get; set; }
    }
}
