using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteDB;
namespace Runas
{
    internal class Prog
    {
       private BsonMapper mapper = BsonMapper.Global;
       private void m()
        {
            mapper.Entity<Prog>().
                Field(x => x.File, "File").Field(x => x.Arguments, "Arguments");
        }
        internal string File { get; set; }
        internal string? Arguments { get; set; }
        internal string _id { get; }
        public Prog() { }
        [BsonCtor]
        public Prog(string _id,string File, string Arguments) {
            this._id = _id;
            this.File = File;
            this.Arguments = Arguments;
        }

    }
}
